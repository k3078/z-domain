## 一个超实用的域名价格比对网站

[Compare Prices of All Top-Level Domains | TLD List (tld-list.com)](https://tld-list.com/)

TLD List对国外的绝大部分域名注册商价格均有监控。实时更新，并且同时收集了各种优惠码，自带有货币转换，以美元作为标准，列出当前域名的注册、续费、转出价格。

另外，此网站还可以查询域名的注册情况。不过此功能时常抽风。

单纯从价格上看，Sav、Cosmotown价格都是比较便宜的。可以考虑。

[Sav](https://www.sav.com/domains)

[Cosmotown](https://cosmotown.com/#!/)

## 一些补充说明

1. 以上的网站仅仅对比了价格一项。实际上，不同服务商的稳定程度等也有区别。目前国外比较大的域名注册平台主要有Name.com、namecheap、godaddy、namesilo、谷歌、Cloudflare等。国内主要有腾讯、阿里（万网）等。如果对价格并不是很敏感，可以选择大厂，比较稳定。
2. 国内直接注册域名的价格较贵，可以考虑使用优惠券。



| 服务商    | 价格（五年，.com）         |
| --------- | -------------------------- |
| Name.com  | 65.5+85.2*4=406            |
| Namecheap | 288（首年39）              |
| porkbun   | 58.1*5=291（续费价格不变） |
| namesilo  | 63.3*5=317（续费价格不变） |
| 腾讯      | 341（首年61）              |
| 阿里      | 363（首年63）              |
